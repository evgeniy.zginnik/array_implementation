function MyArray() {
    this.length = 0;
    for (i = 0; i < arguments.length; i++) {
        this[i] = arguments[i];
        this.length++
    }

}

MyArray.prototype[Symbol.iterator] = function () {
    let counter = 0
    return {
        next: () => {
            if (counter < this.length) {
                return {
                    value: this[counter++],
                    done: false
                }
            } else {
                return {
                    done: true
                }
            }
        }
    }
}

MyArray.prototype.map = function (callback) {
    for (i = 0; i < this.length; i++) {
        this[i] = callback(this[i], i, this)
    };
    return this;
};

MyArray.prototype.reduce = function (callback, initValue) {
    if (initValue === undefined) {
        initValue = this[0]
        for (i = 1; i < this.length; i++) {
            initValue = callback(initValue, this[i], i, this)
        };
    } else {
        for (i = 0; i < this.length; i++) {
            initValue = callback(initValue, this[i], i, this)
        };
    }
    return initValue;
};

MyArray.prototype.forEach = function (func) {
    for (i = 0; i < this.length; i++) {
        func(this[i], i, this)
    };
};

MyArray.prototype.filter = function (func) {
    let newObj = new MyArray();
    for (i = 0; i < this.length; i++) {
        (func(this[i], i, this)) ? newObj[newObj.length++] = this[i] : false
    };
    return newObj;
};

MyArray.prototype.push = function () {
    for (i = 0; i < arguments.length; i++) {
        this[this.length++] = arguments[i]
    };
    return this.length;
};

MyArray.prototype.pop = function () {
    let a = this[this.length - 1];
    delete this[this.length - 1];
    this.length--;
    return a;
};

MyArray.prototype.toString = function () {
    let retString = this[0];
    for (i = 1; i < this.length; i++) {
        retString += ',' + this[i]
    };
    return retString;
};

MyArray.prototype.sort = function (callback) {
    if (callback === undefined) {
        console.log(1);
        for (i = 0; i < this.length; i++) {
            for (x = 0; x < this.length; x++) {
                if (`${this[x]}` > `${this[x + 1]}`) {
                    let num = this[x];
                    this[x] = this[x + 1];
                    this[x + 1] = num;
                }
            }
        }
    } else {
        console.log(2);
        for (i = 0; i < this.length; i++) {
            for (x = 0; x < this.length; x++) {
                if (callback(this[x], this[x + 1]) >= 0) {
                    let num = this[x];
                    this[x] = this[x + 1];
                    this[x + 1] = num;
                }
            }
        }
    }
    return this
};

MyArray.from = function (list = undefined, callback) {
    let index = 0;
    for (let key of list) {
        if (callback) {
            pseudoArr[index++] = callback(key, i)
        } else {
            pseudoArr[index++] = key
        }
    }
    pseudoArr.length = index;
    return pseudoArr
};


let arr = new MyArray(1, 4, [2, 4], { name: "Jack" });